<?php

use App\Http\Controllers\view\viewDetailMobilController;
use App\Http\Controllers\view\viewMobilController;
use App\Http\Controllers\view\viewTypeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::name('mobil.brand.')->prefix('/mobil/brand/')->group(function(){
    Route::get('all', [viewMobilController::class, 'index'])->name('all');
    Route::get('create', [viewMobilController::class, 'create'])->name('create');
    Route::get('edit/{id}', [viewMobilController::class, 'edit'])->name('edit');
    Route::post('make', [viewMobilController::class, 'store'])->name('make');
    Route::post('change', [viewMobilController::class, 'update'])->name('change');
    Route::get('destroy/{id}', [viewMobilController::class, 'destroy'])->name('destroy');
});

Route::name('mobil.type.')->prefix('/mobil/type')->group(function(){
    Route::get('all', [viewTypeController::class, 'index'])->name('all'); // to show all data
    Route::get('create', [viewTypeController::class, 'create'])->name('create'); // to create data
    Route::get('edit/{id}', [viewTypeController::class, 'edit'])->name('edit'); // to update data
    Route::post('make', [viewTypeController::class, 'store'])->name('make'); // storing data
    Route::post('change', [viewTypeController::class, 'update'])->name('change'); // store updated data
    Route::get('destroy/{id}', [viewTypeController::class, 'destroy'])->name('destroy'); // delete data
});

Route::name('mobil.type.')->prefix('/mobil/type/')->group(function(){
    Route::get('{id}/spac', [viewDetailMobilController::class, 'index'])->name('spec'); // to show all data
    Route::get('{id}/spec/create', [viewDetailMobilController::class, 'create'])->name('spec.create'); // to create data
    Route::get('{id}/spec/edit', [viewDetailMobilController::class, 'edit'])->name('spec.edit'); // to update data
    Route::post('spec/make', [viewDetailMobilController::class, 'store'])->name('spec.make');
});
