<?php

use App\Http\Controllers\api\apiMobilController;
use App\Http\Controllers\api\apiTypeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('brand', [apiMobilController::class, 'index'])->name('index');
Route::post('brand', [apiMobilController::class, 'store'])->name('store');
Route::post('brand/update', [apiMobilController::class, 'update'])->name('update');
Route::get('brandshow', [apiMobilController::class, 'show'])->name('show');
Route::delete('brand', [apiMobilController::class, 'destroy'])->name('delete');

Route::name('type.')->prefix('type/')->group(function(){
    Route::get('all', [apiTypeController::class, 'index'])->name('index');
    Route::post('create', [apiTypeController::class, 'store'])->name('store');
});
