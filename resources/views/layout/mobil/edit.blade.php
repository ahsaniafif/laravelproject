@extends('page.index')
@section('section-name', 'Edit')
@section('content')
    <form method="post" action="{{ route('mobil.brand.change') }}">
        @csrf
        {{-- @foreach ($data as $item) --}}
        <div class="form-group">
            <label for="cdBrand">Code Brand</label>
            <input type="text" name="cd_brand" id="cdBrand" value="{{ $res['cd_brand'] }}">
            print_r($res)
        </div>
        <div class="form-group">
            <label for="descBrand">Name Brand</label>
            <input type="text" name="desc_brand" id="descBrand" value="{{ $res['desc_brand'] }}">
        </div>
        {{-- @endforeach --}}
        <button type="submit" class="btn btn-primary btn-submit">Save</button>
    </form>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            // console.log(:id);
            $('.btn-submit').click(function (e) {
                e.preventDefault();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "post",
                    url: "{{ url('mobil/brand/change') }}",
                    data: {
                        'cd_brand': $('#cdBrand').val(),
                        'desc_brand': $('#descBrand').val(),
                    },
                    dataType: "json",
                    success: function (response) {
                        window.location.replace("{{ route('mobil.brand.all') }}")
                    }
                });
            });
        });
    </script>
@endsection
