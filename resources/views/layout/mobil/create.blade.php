@extends('page.index')
@section('section-name', 'Create')
@section('content')

    <ul id="saveform_errList"></ul>
    <form method="post">
        @csrf
        <div class="form-group">
            <label for="cdBrand">Code Brand</label>
            <input type="text" name="cd_brand" id="cdBrand">
        </div>
        <div class="form-group">
            <label for="descBrand">Desc Brand</label>
            <input type="text" name="desc_brand" id="descBrand">
        </div>
        <div class="row justify-content-start">
            <button type="submit" class="btn btn-primary btn-submit mr-2">Save</button>
            <a href="{{ route('mobil.brand.all') }}" class="btn btn-primary ">Back</a>
        </div>

    </form>

@endsection

@section('script')
    <script>

        $(document).ready(function () {
            $('.btn-submit').click(function (e) {
                e.preventDefault();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: "{{ route('mobil.brand.make') }}",
                    data: {
                        'cd_brand': $('#cdBrand').val(),
                        'desc_brand': $('#descBrand').val(),
                    },
                    dataType: "json",
                    success: function (response) {
                        // console.log(response);
                        if(response.status == 400){
                            $('#saveform_errList').html("");
                            $('#saveform_errList').addClass('alert alert-danger');
                            $.each(response.message, function (key, err_values) {
                                $('#saveform_errList').append('<li>'+ err_values +'</li>')
                            });
                        }
                        else{
                            $('#saveform_errList').html("");
                            $('#saveform_errList').addClass('alert alert-success');
                            $('#saveform_errList').text(response.message);
                            window.location.replace("{{ route('mobil.brand.all') }}")
                        }
                    }
                });
            });
        });
    </script>
@endsection
