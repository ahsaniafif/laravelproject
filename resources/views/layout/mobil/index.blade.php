@extends('page.index')
@section('section-name', 'Dashboard')
@section('content')
<a href="{{ route('mobil.brand.create') }}" class="btn btn-primary mb-3">Create Brand</a>

{{-- ======modal====== --}}
{{-- <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Deleting Data?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <h6>Are you Sure?</h6>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger">Delete</button>
        </div>
      </div>
    </div>
  </div> --}}
  {{-- ======end Modal====== --}}

<table class="table">
    <thead>
      <tr>
        <th scope="col">Code Brand</th>
        <th scope="col">Name Brand</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($collection as $item)
        <tr>
            <td>{{ $item->cd_brand }}</td>
            <td>{{ $item->desc_brand }}</td>
            <td>
                <a href="{{ url('mobil/brand/edit/'. $item->cd_brand) }}" class="btn btn-warning" id="btnEdit">Edit</a>
                <a href="{{ url('mobil/brand/destroy/'. $item->cd_brand) }}" class="btn btn-danger" id="btnDelete" ">Delete</a>
            </td>
        </tr>
      @endforeach
    </tbody>
</table>
@endsection

@section('script')
    <script>
        // $(document).ready(function () {
        //     $('#btnDelete').click(function (e) {
        //         e.preventDefault();

        //     });
        // });
    </script>
@endsection
