@extends('page.index')
@section('section-name', 'Edit')
@section('content')
    <form action="{{ route('mobil.type.change') }}" method="post">
        @csrf
        @foreach ($data as $item)
            <div class="form-group">
                <label for="cdType">Type Code</label>
                <input type="text" name="cd_type" id="cdType" value="{{ $item->cd_type }}">
            </div>
            <div class="form-group">
                <label for="cdBrand">Brand Code</label>
                <input type="text" name="cd_brand" id="cdBrand" value="{{ $item->cd_brand }}">
            </div>
            <div class="form-group">
                <label for="descType">Type Name</label>
                <input type="text" name="desc_type" id="descType" value="{{ $item->desc_type }}">
            </div>
        @endforeach
        <div class="row justify-content-start">
            <input type="submit" value="save" class="btn btn-primary mr-1">
            <a href="{{ route('mobil.type.all') }}" class="btn btn-primary">Go Back</a>
        </div>
    </form>
@endsection
