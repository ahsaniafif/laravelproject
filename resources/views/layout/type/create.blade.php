@extends('page.index')
@section('section-name', 'create')
@section('content')
    <form method="post" action="{{ route('mobil.type.make') }}">
        @csrf
        <div class="form-group">
            <label for="cdBrand" class="mr-2">Brand Select</label>
            <select name="cd_brand" id="cdBrand" class="btn btn-secondary dropdown-toggle" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" type="button">
                <div class="dropdown-menu" aria-labelledby="cdBrand">
                    @foreach ($data as $item)
                        <option value="{{ $item->cd_brand }}" class="dropdown-item text-light">{{ $item->desc_brand }}</option>
                    @endforeach
                </div>
            </select>
        </div>

        <div class="form-group">
            <label for="cdType" class="mr-2">Type Code</label>
            <input type="text" name="cd_type" id="cdType">
        </div>

        <div class="form-group">
            <label for="descType" class="mr-2">Type Name</label>
            <input type="text" name="desc_type" id="descType">
        </div>

        <div class="row justify-content-start">
            {{-- <button type="submit" class="btn btn-primary btn-submit mr-2">Save</button> --}}
            <input type="submit" value="Save" class="btn btn-primary mr-2">
            <a href="{{ route('mobil.type.all') }}" class="btn btn-primary ">Back</a>
        </div>
    </form>
@endsection
