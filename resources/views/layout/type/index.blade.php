@extends('page.index')
@section('section-name', 'Type')
@section('content')
<a href="{{ route('mobil.type.create') }}" class="btn btn-primary mb-3">Create Type</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">Code Type</th>
        {{-- <th scope="col">Brand</th> --}}
        <th scope="col">Type</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($datas as $item)
        <tr>
            <td>{{ $item->cd_type }}</td>
            {{-- <td>{{ $item->desc_brand }}</td> --}}
            <td>{{ $item->desc_type }}</td>
            <td>
                <a href="{{ route('mobil.type.spec', $item->cd_type) }}" class="btn btn-primary">Detail</a>
                <a href="{{ url('mobil/type/edit/'. $item->cd_type) }}" class="btn btn-warning" id="btnEdit">Edit</a>
                <a href="{{ route('mobil.type.destroy', $item->cd_type) }}" class="btn btn-danger" id="btnDelete" ">Delete</a>
            </td>
        </tr>
      @endforeach
    </tbody>
</table>
@endsection
