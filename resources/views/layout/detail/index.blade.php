@extends('page.index')
@section('section-name', 'Details')
@section('content')

@foreach ($joinBrandType as $item)
    <a href="{{ route('mobil.type.spec.create', $item->cd_type) }}">Add Spec</a>
    <h4>Brand : <span>{{ $item->desc_brand }}</span></h4>
    <h4>Type : <span>{{ $item->desc_type }}</span></h4>
@endforeach
<table class="table">
    <thead>
      <tr>
        <th scope="col">Year</th>
        {{-- <th scope="col">Brand</th> --}}
        <th scope="col">Price</th>
        <th scope="col">Spec</th>
        <th scope="col">Photo</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($detail as $item)
        <tr>
            <td>{{ $item->year }}</td>
            <td>{{ $item->price }}</td>
            <td>{{ $item->spec }}</td>
            <td>{{ $item->photo }}</td>
            <td>
                <a href="{{ url('mobil/type/edit/'. $item->cd_type) }}" class="btn btn-warning" id="btnEdit">Edit</a>
                <a href="{{ route('mobil.type.destroy', $item->cd_type) }}" class="btn btn-danger" id="btnDelete" ">Delete</a>
            </td>
        </tr>
      @endforeach
    </tbody>
</table>
@endsection
