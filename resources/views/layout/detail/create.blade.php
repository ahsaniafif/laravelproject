@extends('page.index')
@section('section-name', 'Detail Create')
@section('content')
<form method="post" action="{{ route('mobil.type.spec.make') }}">
    @csrf
    <div class="form-group row">
        <label for="cdBrand" class="mr-2 col-md-2">Brand Select</label>
        <select name="cd_brand" id="cdBrand" class="btn btn-secondary dropdown-toggle col-md-2" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" type="button">
            <div class="dropdown-menu" aria-labelledby="cdBrand">
                <option value="">--Select Brand--</option>
                @foreach ($data as $item)
                    <option value="{{ $item->cd_brand }}" class="dropdown-item text-light">{{ $item->desc_brand }}</option>
                @endforeach
            </div>
        </select>
    </div>

    <div class="form-group row">
        <label for="cdType" class="mr-2 col-md-2">Type Select</label>
        <select name="cd_type" id="cdBrand" class="btn btn-secondary dropdown-toggle col-md-2" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" type="button">
            <div class="dropdown-menu" aria-labelledby="cdType">
                <option value="">--Select Brand--</option>
                @foreach ($data as $item)
                    <option value="{{ $item->cd_type }}" class="dropdown-item text-light">{{ $item->desc_type }}</option>
                @endforeach
            </div>
        </select>
    </div>

    <div class="form-group row">
        <label for="year" class="mr-2 col-md-2">Year</label>
        <input type="text" name="year" id="year" class="col-md-4">
    </div>
    <div class="form-group row">
        <label for="price" class="mr-2 col-md-2">Price</label>
        <input type="text" name="price" id="price" class="col-md-4">
    </div>
    <div class="form-group row">
        <label for="spec" class="mr-2 col-md-2">Specification</label>
        <input type="text" name="spec" id="spec" class="col-md-4">
    </div>

    <div class="row justify-content-start">
        {{-- <button type="submit" class="btn btn-primary btn-submit mr-2">Save</button> --}}
        <input type="submit" value="Save" class="btn btn-primary mr-2">
        <a href="{{ route('mobil.type.all') }}" class="btn btn-primary ">Back</a>
    </div>
</form>
@endsection
