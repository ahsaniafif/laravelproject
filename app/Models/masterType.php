<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class masterType extends Model
{
    use HasFactory;
    protected $table = 'type';
    protected $fillable = [
        'cd_brand',
        'cd_type',
        'desc_type'
    ];
}
