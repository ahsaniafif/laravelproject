<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class masterBrand extends Model
{
    use HasFactory;
    protected $table = 'brand';
    protected $fillable=[
        'cd_brand',
        'desc_brand'
    ];

    public function getdata()
    {
        return DB::table('brand')->get();
    }
}
