<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailMobil extends Model
{
    use HasFactory;
    protected $table = 'detailmobil';
    protected $fillable = [
        'cd_brand',
        'cd_type',
        'year',
        'price',
        'spec',
        'photo',
    ];
}
