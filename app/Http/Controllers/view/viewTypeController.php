<?php

namespace App\Http\Controllers\view;

use App\Http\Controllers\Controller;
use App\Models\masterBrand;
use App\Models\masterType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class viewTypeController extends Controller
{
    public function index()
    {
        // $data = DB::table('type')
        //         ->join('brand', 'type.cd_brand', '=', 'brand.cd_brand')
        //         ->select('type.*', 'brand.desc_brand')
        //         ->get();
        $data = masterType::get();
        return view('layout.type.index', ['datas' => $data]);
    }

    public function create()
    {
        $data = masterBrand::get();
        return view('layout.type.create', compact('data'));
    }

    public function store(Request $request)
    {

        $data = new masterType;
        $data->cd_type = $request->cd_type;
        $data->cd_brand = $request->cd_brand;
        $data->desc_type = $request->desc_type;
        $data->save();

        return redirect()->route('mobil.type.all');
    }
    public function edit($id)
    {
        $data = masterType::where(['cd_type' => $id])->get();
        return view('layout.type.edit', ['data' => $data]);
    }

    public function update(Request $request)
    {
        $data = masterType::where(['cd_type' => $request->cd_type])->update(['desc_type' => $request->desc_type]);
        return redirect()->route('mobil.type.all');
    }

    public function destroy($id)
    {
        masterType::where('cd_type', $id)->delete();
        return redirect()->route('mobil.type.all');
    }

}
