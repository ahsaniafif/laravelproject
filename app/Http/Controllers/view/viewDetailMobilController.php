<?php

namespace App\Http\Controllers\view;

use App\Http\Controllers\Controller;
use App\Models\DetailMobil;
use App\Models\masterBrand;
use App\Models\masterType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class viewDetailMobilController extends Controller
{
    public function index($id)
    {
        $joinBrandType = masterBrand::join('type', 'brand.cd_brand', '=', 'type.cd_brand')->where('type.cd_type', $id)->get();
        $detail = DetailMobil::where('cd_type', $id)->get();
        return view('layout.detail.index', [
            'joinBrandType' => $joinBrandType,
            'detail' => $detail,
        ]);
        // dd($joinBrandType);
    }

    public function create($id)
    {
        $data = masterBrand::join('type', 'brand.cd_brand', '=', 'type.cd_brand')->get()->all();
        return view('layout.detail.create', ['data' => $data]);
    }

    public function store(Request $request)
    {
        $rule = [
            'cd_brand' => 'required',
            'cd_type' => 'required',
            'year' => 'required|min:4|max:4',
            'price' => 'required',
        ];

        $validator = validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return $validator->getMessageBag();
        }
        else {
            $detailData = new DetailMobil;
            $detailData->cd_brand = $request->cd_brand;
            $detailData->cd_type = $request->cd_type;
            $detailData->year = $request->year;
            $detailData->price = $request->price;
            $detailData->spec = $request->spec;
            $detailData->save();

            return redirect()->route('mobil.type.spec');
        }
    }
}
