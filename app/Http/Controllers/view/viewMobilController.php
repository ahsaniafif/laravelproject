<?php

namespace App\Http\Controllers\view;

use App\Http\Controllers\Controller;
use App\Models\masterBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class viewMobilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->masterBrand = new masterBrand();
    }

    public function index()
    {
        $collection = masterBrand::get();
        return view('layout.mobil.index', ['collection' => $collection]);
        // dd($collection);
    }

    // public function showall()
    // {
    //     $collection = masterBrand::all();
    //     return response()->json($collection);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layout.mobil.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'cd_brand' => 'required|min:3',
            'desc_brand' => 'required|max:10',
        ];

        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->getMessageBag(),
            ]);
        } else {
            $createBrand = new masterBrand;
            $createBrand->cd_brand = $request->cd_brand;
            $createBrand->desc_brand = $request->desc_brand;
            $createBrand->save();

            return response()->json([
                'status' => 200,
                'message' => 'Successfully added'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $data = masterBrand::where('cd_brand', $id)->get();
        // dd(value($data->cd_brand));
        $arr = [];
        $keys = ['cd_brand', 'desc_brand'];

        foreach ($data as $key => $value) {
            // dd($value->cd_brand);
            array_push($arr, $value->cd_brand, $value->desc_brand);
        }
        $res = array_combine($keys,$arr);
        return view('layout.mobil.edit', compact('res'));
        // dd($res);
        // dd(count($data));
        // exception Property [cd_brand] does not exist on this collection instance (at edit blade)
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = masterBrand::where('cd_brand', $request->cd_brand)->update(['desc_brand' => $request->desc_brand]);
        // return view('layout.mobil.index');
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        masterBrand::where('cd_brand', $id)->delete();
        return redirect()->route('mobil.brand.all');
    }
}
