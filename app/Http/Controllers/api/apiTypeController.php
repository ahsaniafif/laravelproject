<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\masterType;
use Illuminate\Http\Request;

class apiTypeController extends Controller
{
    public function index()
    {
        $data = masterType::all();
        return $data;
    }
    public function store(Request $request)
    {
        masterType::create($request->all());
    }
}
